from distutils.core import setup
import os.path


setup(
    name="shakemap_aqms",
    version=1.0,
    description="AQMS Modules for ShakeMap",
    author="Bruce Worden",
    author_email="cbworden@usgs.gov",
    maintainer="Li Zhang",
    maintainer_email="lizhang@contractor.usgs.gov",
    url="https://gitlab.com/aqms-swg/aqms-shakemap",
    packages=["shakemap_aqms", "shakemap_aqms.coremods",],
    package_data={"shakemap_aqms": [os.path.join("config", "*")]},
    scripts=[],
)
